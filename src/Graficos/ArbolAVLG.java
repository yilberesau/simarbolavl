/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Graficos;

import Colecciones_SEED.NodoAVL;
import Colecciones_SEED.Pila;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author usuario
 */
public class ArbolAVLG extends ArbolBBG {
    
    private Pila<Label> alts;

    public ArbolAVLG() {
        super();
        this.alts = new Pila<Label>();
    }

    public Pila<Label> getAlts() {
        return alts;
    }

    public void setAlts(Pila<Label> alts) {
        this.alts = alts;
    }
    
    /**
     *
     * @param raiz
     */
    public void crearArbol(NodoAVL raiz) {
        super.setRaiz(crear(raiz));        
    }
    
    private NodoBinG crear(NodoAVL r) {
        if(r==null)
            return (null);
        else
        {
            NodoBinG aux=new NodoAVLG((int) r.getInfo(), super.getPila().desapilar(), super.getLab().desapilar(), this.alts.desapilar(), crear(r.getIzq()), crear(r.getDer()), r.getBal());
            return (aux);
        }
    }

    public void cargarAlturas(Label[] alt) {
        for(int i=0; i<alt.length; i++)
            this.alts.apilar(alt[i]);
    }
    
    @Override
    public void pintarArbol(int alt){
        if(super.getRaiz()==null){
            Circle c = super.getPila().desapilar();
            Label l = super.getLab().desapilar();   
            c.setLayoutX(512.0);
            c.setLayoutY(195.0);
            l.setLayoutX(501.0);
            l.setLayoutY(189.0);
            c.setFill(Color.GRAY);
            c.setStroke(Color.BLACK);
            l.setText("Null");            
            c.setVisible(true);
            l.setVisible(true);
            return;
        }
        int ajuste = 0;
        switch(alt){
            case 0: ajuste = 0; break;
            case 1: ajuste = 1; break;
            case 2: ajuste = 2; break;
            case 3: ajuste = 4; break;
            case 4: ajuste = 8; break;
            case 5: ajuste = 16;break;
            case 6: ajuste = 32;break;
            case 7: ajuste = 64;break;
        }
        this.pinta((NodoAVLG) super.getRaiz(),null,ajuste);
        this.reajustarColores();
    }

    private void pinta(NodoAVLG r, NodoAVLG padre, int ajuste) {        
        Line l;        
        ajuste = ajuste/2;
        if(r==null){
            return;
        }        
        r.getC().setVisible(true);
        r.getL().setText(r.getInfo()+"");
        r.getL().setVisible(true);  
        r.getL2().setText(r.getBal()+"");
        r.getL2().setVisible(true);  
        
        if(padre==null){
            r.getC().setLayoutX(512.0);
            r.getC().setLayoutY(195.0);
            r.getL().setLayoutX(501.0);
            r.getL().setLayoutY(189.0);
            r.getL2().setLayoutX(501.0);
            r.getL2().setLayoutY(210.0);
            r.getC().setVisible(true);
            r.getL().setText(r.getInfo()+"");
            r.getL().setVisible(true);
            r.getL2().setText(r.getBal()+"");
            r.getL2().setVisible(true);
        }
        if(r.getIzq()!=null){
            r.getIzq().getC().setLayoutX(r.getC().getLayoutX()-(15*ajuste));
            r.getIzq().getC().setLayoutY(r.getC().getLayoutY()+50.0);
            r.getIzq().getL().setLayoutX(r.getL().getLayoutX()-(15*ajuste));
            r.getIzq().getL().setLayoutY(r.getL().getLayoutY()+50.0);
            r.getIzq().getL2().setLayoutX(r.getL2().getLayoutX()-(15*ajuste));
            r.getIzq().getL2().setLayoutY(r.getL2().getLayoutY()+50.0);
            r.getIzq().getC().setVisible(true);
            r.getIzq().getL().setText(r.getInfo()+"");
            r.getIzq().getL().setVisible(true);            
            r.getIzq().getL2().setText(r.getBal()+"");
            r.getIzq().getL2().setVisible(true);
            l = super.getLin().desapilar();            
            l.setLayoutX(r.getC().getLayoutX());
            l.setLayoutY(r.getC().getLayoutY());
            l.setStartX(0.0);
            l.setEndX(-(15*ajuste));
            l.setStartY(0.0);
            l.setEndY(50.0);
            l.setVisible(true);
            
        }
        if(r.getDer()!=null){
            r.getDer().getC().setLayoutX(r.getC().getLayoutX()+(15*ajuste));
            r.getDer().getC().setLayoutY(r.getC().getLayoutY()+50.0);
            r.getDer().getL().setLayoutX(r.getL().getLayoutX()+(15*ajuste));
            r.getDer().getL().setLayoutY(r.getL().getLayoutY()+50.0);
            r.getDer().getL2().setLayoutX(r.getL2().getLayoutX()+(15*ajuste));
            r.getDer().getL2().setLayoutY(r.getL2().getLayoutY()+50.0);
            r.getDer().getC().setVisible(true);
            r.getDer().getL().setText(r.getInfo()+"");
            r.getDer().getL().setVisible(true);
            r.getDer().getL2().setText(r.getBal()+"");
            r.getDer().getL2().setVisible(true);
            l = super.getLin().desapilar();                        
            l.setLayoutX(r.getC().getLayoutX());
            l.setLayoutY(r.getC().getLayoutY());
            l.setStartX(0.0);
            l.setEndX((15*ajuste));
            l.setStartY(0.0);
            l.setEndY(50.0);
            l.setVisible(true);            
        }
        pinta(r.getIzq(),r,ajuste);
        pinta(r.getDer(),r,ajuste);
    }

}
