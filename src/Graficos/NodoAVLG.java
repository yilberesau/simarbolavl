/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Graficos;

import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

/**
 *
 * @author usuario
 */
public class NodoAVLG extends NodoBinG {
    
    private int bal;
    private Label l2;

    public NodoAVLG(int info, Circle c, Label l, Label l2, NodoBinG izq, NodoBinG der, int b) {
        super(info, c, l, izq, der);
        this.bal = b;
        this.l2 = l2;
    }
    
    /**
     *
     * @return
     */
    @Override
    public NodoAVLG getIzq() {
        return (NodoAVLG) super.getIzq();
    }

    /**
     *
     * @param izq
     */
    public void setIzq(NodoAVLG izq) {
        super.setIzq(izq);
    }

    /**
     *
     * @return
     */
    @Override
    public NodoAVLG getDer() {
        return (NodoAVLG) super.getDer();
    }

    /**
     *
     * @param der
     */
    public void setDer(NodoAVLG der) {
        super.setDer(der);
    }

    
    

    public int getBal() {
        return bal;
    }

    public void setBal(int bal) {
        this.bal = bal;
    }

    public Label getL2() {
        return l2;
    }

    public void setL2(Label l2) {
        this.l2 = l2;
    }
    
    
    
    
    
    
}
