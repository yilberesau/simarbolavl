/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Graficos;

import Colecciones_SEED.ListaCD;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.paint.Color;
import javafx.util.Duration;
/**
 *
 * @author usuario
 */
public class ArbolBBG extends ArbolBinarioG{

    public ArbolBBG() {
        super();
    }
    
    /**
     *
     * @param i 
     * @param dato
     * @param tl  
     */
    @Override
    public void animacion(int i, int dato, Timeline tl) {
        this.reajustarColores();
        tl = new Timeline();
        tl.setCycleCount(1);
        tl.getKeyFrames().removeAll();
        switch(i){
            //Insertar 0
            case 0:
                this.pintarInsertado(dato, tl);
                break;
            //Eliminar 1
            case 1:                
                break;
            //Buscar 2
            case 2:
                this.buscarG(dato, tl);
                break;
            //Hojas 3
            case 3:
                this.getHojas();
                break;
            //Podar 4
            case 4:
                this.getHojas();
                break;
            // preorden 5
            case 5:
                this.preOrden(tl);
                break;
            // inorden 6
            case 6:
                this.inOrden(tl);
                break;
            //postorden 7
            case 7:
                this.postOrden(tl);
                break;
            //niveles 8
            case 8:
                this.impNiveles(tl);
                break;
        }
    }
    
    /**
     *
     * @param dato
     * @param tl
     */
    @Override
    public void pintarInsertado(int dato, Timeline tl){
        double durac = 0;
        NodoBinG n = this.buscar(super.getRaiz(), dato);
        if(n!=null){
            tl.getKeyFrames().setAll(
            new KeyFrame(new Duration(durac), new KeyValue(n.getC().strokeProperty(),Color.GREEN)),
            new KeyFrame(new Duration((durac+1000)), new KeyValue(n.getC().strokeProperty(),Color.RED)),
            new KeyFrame(new Duration(durac), new KeyValue(n.getC().fillProperty(),Color.GREENYELLOW)),
            new KeyFrame(new Duration((durac+1000)), new KeyValue(n.getC().fillProperty(),Color.BEIGE)));
            tl.play();
        }
    }
    
    private NodoBinG buscar(NodoBinG r, int x){
        if (r==null)
            return (null);
        int compara=((Comparable)r.getInfo()).compareTo(x);
        if(compara>0)
            return(buscar(r.getIzq(),x));
        else
            if(compara<0)
                return(buscar(r.getDer(),x));                            
            else
                return (r);
    }
    
    /**
     *
     * @param x
     * @param tl
     * @return
     */
    @Override
    public boolean buscarG(int x, Timeline tl){
        ListaCD<NodoBinG> l=new ListaCD<NodoBinG>();
        boolean rta = this.buscarG(super.getRaiz(), x, l);
        int durac = 0;
        for(int i=0; i<l.getTamanio(); i++){
            NodoBinG n = l.get(i);
            tl.getKeyFrames().addAll(new KeyFrame(new Duration(durac), new KeyValue(n.getC().strokeProperty(),Color.GREEN)),
            new KeyFrame(new Duration((durac+1000)), new KeyValue(n.getC().strokeProperty(),Color.RED)));
            tl.getKeyFrames().addAll(new KeyFrame(new Duration(durac), new KeyValue(n.getC().fillProperty(),Color.GREENYELLOW)),
            new KeyFrame(new Duration((durac+1000)), new KeyValue(n.getC().fillProperty(),Color.BEIGE)));
            durac+=1000;
        }
        tl.play();
        return (rta);
    }
    
    private boolean buscarG(NodoBinG r, int x, ListaCD<NodoBinG> l){
        if (r==null)
            return (false);
        l.insertarAlFinal(r);
        int compara=((Comparable)r.getInfo()).compareTo(x);
        if(compara>0)
            return(buscarG(r.getIzq(),x,l));
        else
            if(compara<0)
                return(buscarG(r.getDer(),x,l));                            
            else
                return (true);
    }
   
    
}
