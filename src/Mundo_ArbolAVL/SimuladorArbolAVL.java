/**
 * ---------------------------------------------------------------------
 * $Id: SimuladorArbolAVL.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Mundo_ArbolAVL;


import Colecciones_SEED.ArbolAVL;
import Colecciones_SEED.NodoAVL;
import Graficos.ArbolAVLG;
import java.util.Iterator;

/**
 * Clase que conecta la capa de presentación del Simulador con las Estructuras de Datos.
 * @author Uriel Garcia - Yulieth Pabon
 * @version 1.0
 */

public class SimuladorArbolAVL {
    
    private ArbolAVL<Integer> miAVL;
    
    public SimuladorArbolAVL(){
        this.miAVL = new ArbolAVL<Integer>();
    }
    
    public ArbolAVLG crearArbolAVLGrafico(ArbolAVLG n) {
        n.crearArbol((NodoAVL)this.miAVL.getRaiz());
        return (n);
    }

    public int conocerAltura() {
        return this.miAVL.getAltura();
    }

    public int insertarDato(int dato) {
        if(this.miAVL.esta(dato))
            return -1; //Dato repetido
        if(this.miAVL.insertar(dato)){  
            if(this.miAVL.getAltura()>5){
                this.miAVL.eliminar(dato);
                return (-2); //Supera la altura
            }
            return 0; //Insercion correcta
        }
        return -3; //No se pudo insertar
    }

    public boolean estaVacioArbol() {
        return (this.miAVL.esVacio());
    }

    public boolean eliminarDato(int dato) {
        return (this.miAVL.eliminar(dato));
    }

    public boolean estaDatoenArbol(int dato) {
        return (this.miAVL.estaABB(dato));
    }

    public int conocerPeso() {
        return (this.miAVL.getPeso());
    }

    public int contarHojas() {
        return (this.miAVL.contarHojas());
    }

    public String obtenerHojas() {
        Iterator<Integer> it = this.miAVL.getHojas();
        String cad = "";
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
        }        
        return (cad);
    }

    public void podarHojas() {
        this.miAVL.podar();
        this.miAVL.balancearAltura();
    }

    public String recorridoPreorden() {
        Iterator<Integer> it = this.miAVL.preOrden();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
    public String recorridoInorden() {
        Iterator<Integer> it = this.miAVL.inOrden();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
    public String recorridoPostorden() {
        Iterator<Integer> it = this.miAVL.postOrden();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
    public String recorridoPorNiveles() {
        Iterator<Integer> it = this.miAVL.impNiveles();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
}
